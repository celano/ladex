* BaseX/XQuery
  * http://basex.org/ (main website)
  * http://files.basex.org/releases/ (releases)
  * http://docs.basex.org/wiki/Main_Page (documentation)
* Perseus Treebank
  * https://perseusdl.github.io/treebank_data/
  * https://github.com/PerseusDL/treebank_data/blob/master/AGDT2/guidelines/Greek_guidelines.md (Ancient Greek guidelines)
* Reference book on XQuery
  * Walmsley Priscilla . 2016. XQuery: Search Across a Variety of XML Data, 
    O'Reilly Media (2nd edition)
    <br/> (http://www.datypic.com/books/xquery/examples.html)
* Unicode-related resources
  * https://www.unicode.org/versions/
  * https://www.unicode.org/charts/ (charts)
  * https://www.unicode.org/Public/ (resources to download)
* Universal Dependencies
  * https://universaldependencies.org/ (main website/guidelines)
  * https://github.com/UniversalDependencies (Github repository)
* Webanno 
  * https://www.informatik.uni-leipzig.de/webanno/welcome.html?0 (LU instance)
  * https://webanno.github.io/webanno/documentation/ (documentation)
* XQuery-related specifications
  * https://www.w3.org/TR/xml/ 
  * https://www.w3.org/TR/xquery-31/
  * https://www.w3.org/TR/xpath-datamodel-31/
  * https://www.w3.org/TR/xpath-31/
  * https://www.w3.org/TR/xpath-functions-31/
  * https://www.w3.org/TR/xmlschema-2/
* XQuery 1.0 functions (useful examples)
  * http://www.xqueryfunctions.com/xq/alpha.html