xquery version "3.1";

let $target:="http://lindat.mff.cuni.cz/services/udpipe/api/process"
let $params:=map{
                 "model": "english", 
                 "tokenizer":"",  
                 "tagger":"", 
                 "parser":"",
                 "data":"This is an example."
}
return http:send-request(<http:request method='get'/>,
web:create-url($target,$params))