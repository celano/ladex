* Introduction to the course
  * enrollment formalities
  * program of the course
  * what is linguistic annotation
  * what is XQuery, why XQuery
  * hands-on BaseX Tutorial
* XQuery foundations I
  * BaseX as XQuery processor
  * introduction to XML: XML 1.0, XML 1.1, namespaces
  * XPath: axes
  * XPath: predicates
  * annotation tools: Webanno/Arethusa
* XQuery foundations II
  * XPath: axes II
  * XPath: predicates II
* XQuery foundations III
  * XQuery data model (nodes and atomic values)
* XQuery foundations IV
  * XQuery data model (maps array and functions)
* XQuery foundations V
  * FLOWER expression
  * direct constructors
  * string functions
  * handling txt files in XQuery
* The Perseus Treebank
  * what is a treebank
  * the annotation scheme/format
  * querying the Perseus Treebank with XQuery 
* Linguistic annotation
  * what is a linguistic corpus: representativeness and balance
  * treebanking: the case of UD
  * layers of linguistic annotation
  * working with UD in XQuery
* The UD annotation scheme
  * tokenization
  * POS tagging
  * syntactic parsing
  * stand-off annotation: PAULA XML
* The MATE model: Model, Annotate, Train, Test, Evaluate, Revise
* The case of UDPipe
  * prepopulating texts
  * annotation examples
  * BaseX fetch module
* Maps and arrays
  * JSON
* High-order functions

 